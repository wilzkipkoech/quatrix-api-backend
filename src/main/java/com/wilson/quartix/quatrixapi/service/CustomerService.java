package com.wilson.quartix.quatrixapi.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wilson.quartix.quatrixapi.controller.CustomerController;
import com.wilson.quartix.quatrixapi.model.Customer;
import com.wilson.quartix.quatrixapi.repository.CustomerRepository;



@Service
public interface CustomerService {
	
	
	
	 List<Customer> getAllCustomers();
	 
	
	 void saveCustomer(Customer customer);
	
	

}
