package com.wilson.quartix.quatrixapi.model;



import java.io.Serializable;
import java.util.List;

public class JwtRequest implements Serializable {

    /**
	 *
	 */


	private static long serialVersionUID = 5926468583005150707L;

    private String phone;

    private String password;
    
    
    private List<Customer> cus;

    /**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the cus
	 */
	public List<Customer> getCus() {
		return cus;
	}

	/**
	 * @param cus the cus to set
	 */
	public void setCus(List<Customer> cus) {
		this.cus = cus;
	}

	public JwtRequest()

    {

    }

    public JwtRequest(String username, String password) {

        this.setUsername(username);

        this.setPassword(password);

    }

    public String getUsername() {

        return this.phone;

    }

    public void setUsername(String username) {

        this.phone = phone;

    }

    public String getPassword() {

        return this.password;

    }

    public void setPassword(String password) {

        this.password = password;

    }

}
