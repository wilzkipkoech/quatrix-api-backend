package com.wilson.quartix.quatrixapi.model;

import javax.persistence.*;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.bind.annotation.CrossOrigin;

@Entity

@Table(name = "customers")
@EntityScan("com.wilson.quartix.quatrixapi")
public class Customer {
	
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private int id;

	 
	  private String customer_first_name;
	  
	  private String personnel_first_name;
	  
	  private  String personnel_other_name;
	  
	  private String customer_last_name;
	  
	  private String customer_phone;
	  
	  private int agentId;
	  
	  private  String assigned;
	  
	  private String  in_progress;
	  
	  private String completed;
	   
	  private String  deferred;
	   
	  private String status;
	    
	  private String location;
	    
	  private String gender;
	    
	  private String age;
	    
	  private String access_code;
	    
	  private String mpesa;
	    
	  private String autoplay;
	    
	  private String comments;
	    
	  private String registration;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the customer_first_name
	 */
	public String getCustomer_first_name() {
		return customer_first_name;
	}

	/**
	 * @param customer_first_name the customer_first_name to set
	 */
	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}

	/**
	 * @return the personnel_first_name
	 */
	public String getPersonnel_first_name() {
		return personnel_first_name;
	}

	/**
	 * @param personnel_first_name the personnel_first_name to set
	 */
	public void setPersonnel_first_name(String personnel_first_name) {
		this.personnel_first_name = personnel_first_name;
	}

	/**
	 * @return the personnel_other_name
	 */
	public String getPersonnel_other_name() {
		return personnel_other_name;
	}

	/**
	 * @param personnel_other_name the personnel_other_name to set
	 */
	public void setPersonnel_other_name(String personnel_other_name) {
		this.personnel_other_name = personnel_other_name;
	}

	/**
	 * @return the customer_last_name
	 */
	public String getCustomer_last_name() {
		return customer_last_name;
	}

	/**
	 * @param customer_last_name the customer_last_name to set
	 */
	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}

	/**
	 * @return the customer_phone
	 */
	public String getCustomer_phone() {
		return customer_phone;
	}

	/**
	 * @param customer_phone the customer_phone to set
	 */
	public void setCustomer_phone(String customer_phone) {
		this.customer_phone = customer_phone;
	}

	/**
	 * @return the agentId
	 */
	public int getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	/**
	 * @return the assigned
	 */
	public String getAssigned() {
		return assigned;
	}

	/**
	 * @param assigned the assigned to set
	 */
	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	/**
	 * @return the in_progress
	 */
	public String getIn_progress() {
		return in_progress;
	}

	/**
	 * @param in_progress the in_progress to set
	 */
	public void setIn_progress(String in_progress) {
		this.in_progress = in_progress;
	}

	/**
	 * @return the completed
	 */
	public String getCompleted() {
		return completed;
	}

	/**
	 * @param completed the completed to set
	 */
	public void setCompleted(String completed) {
		this.completed = completed;
	}

	/**
	 * @return the deferred
	 */
	public String getDeferred() {
		return deferred;
	}

	/**
	 * @param deferred the deferred to set
	 */
	public void setDeferred(String deferred) {
		this.deferred = deferred;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return the access_code
	 */
	public String getAccess_code() {
		return access_code;
	}

	/**
	 * @param access_code the access_code to set
	 */
	public void setAccess_code(String access_code) {
		this.access_code = access_code;
	}

	/**
	 * @return the mpesa
	 */
	public String getMpesa() {
		return mpesa;
	}

	/**
	 * @param mpesa the mpesa to set
	 */
	public void setMpesa(String mpesa) {
		this.mpesa = mpesa;
	}

	/**
	 * @return the autoplay
	 */
	public String getAutoplay() {
		return autoplay;
	}

	/**
	 * @param autoplay the autoplay to set
	 */
	public void setAutoplay(String autoplay) {
		this.autoplay = autoplay;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the registration
	 */
	public String getRegistration() {
		return registration;
	}

	/**
	 * @param registration the registration to set
	 */
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	    
	 
	  
	  
	    
	  

}
