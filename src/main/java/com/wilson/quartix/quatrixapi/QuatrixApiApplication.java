package com.wilson.quartix.quatrixapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan
@SpringBootApplication
public class QuatrixApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuatrixApiApplication.class, args);
	
	}
	
	
	

}
