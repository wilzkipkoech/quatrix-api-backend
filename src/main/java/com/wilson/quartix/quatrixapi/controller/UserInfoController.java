package com.wilson.quartix.quatrixapi.controller;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wilson.quartix.quatrixapi.exceptions.ValidationException;
import com.wilson.quartix.quatrixapi.model.UserInfo;
import com.wilson.quartix.quatrixapi.repository.UserInfoRepository;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

@RestController
public class UserInfoController {


    final
    private UserInfoRepository userInfoRepository;

//    private HashData hashData = new HashData();

    public UserInfoController(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }


    @PostMapping("/user")
    public Boolean create(@RequestBody Map<String, String> body) throws NoSuchAlgorithmException {
        String phone = body.get("phone");
        if (userInfoRepository.existsByphone(phone)){

            throw new ValidationException("User with the phone Number Already Exist");

        }

        String password = body.get("password");
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
//        String hashedPassword = hashData.get_SHA_512_SecurePassword(password);
  
        userInfoRepository.save(new UserInfo(phone, encodedPassword));
        return true;
    }

}